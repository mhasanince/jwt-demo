const config = require('config.json');
const jwt = require('jsonwebtoken');

// users hardcoded for simplicity, store in a db for production applications
const users = [
  {
    id: 1,
    username: 'test',
    password: 'test',
    firstName: 'Test',
    lastName: 'User',
  },
  {
    id: 2,
    username: 'ipek',
    password: '123',
    firstName: 'Ipek',
    lastName: 'Efendiev',
  },
  {
    id: 3,
    username: 'mami',
    password: '456',
    firstName: 'Muhammed',
    lastName: 'Ince',
  },
  {
    id: 4,
    username: 'umut',
    password: '789',
    firstName: 'Umut',
    lastName: 'Gurbuz',
  },
];

async function authenticate({ username, password }) {
  const user = users.find(
    (u) => u.username === username && u.password === password
  );

  if (!user) throw 'Username or password is incorrect';

  // create a jwt token that is valid for 7 days
  const token = jwt.sign({ sub: user.id }, config.secret, { expiresIn: '7d' });

  return {
    ...omitPassword(user),
    token,
  };
}

async function getAll() {
  return users.map((user) => omitPassword(user));
}

// helper functions

function omitPassword(user) {
  const { password, ...userWithoutPassword } = user;
  return userWithoutPassword;
}

module.exports = {
  authenticate,
  getAll,
};
